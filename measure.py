#!/usr/bin/python3
import random

# set seed to get same results
random.seed(935)

PT = 2  # profile measurement tolerance
N = 5  # number of consecutive measurement
X = 0.8  # percent of profile error


def measure_top_sensor(i: int) -> float:
    """
    sensor measurement function for simulation
    """
    return 10 + round(random.random(), 3) + random.randint(0, 2)


def measure_bottom_sensor(i: int) -> float:
    """
    sensor measurement function for simulation
    """
    return 10 + round(random.random(), 3) + random.randint(0, 2)


def measure_profile():
    total = 24  # number of total measurement per slab
    num_sensors = 3  # number of sensors at top and bottom
    top_profile = []
    bottom_profile = []
    for _ in range(total):
        # each measurement must wait for encoder in real application

        top_measures = []
        bottom_measures = []

        for i in range(num_sensors):
            v = measure_top_sensor(i)
            top_measures.append(v)

            v = measure_bottom_sensor(i)
            bottom_measures.append(v)

        top_profile.append(abs(min(top_measures) - max(top_measures)))
        bottom_profile.append(abs(min(bottom_measures) - max(bottom_measures)))

        if len(top_profile) >= N:
            x = sum((1 for m in top_profile[-N:] if m > PT))/N # calculate percentage of out of tolerance conditions
            if x >= X:
                print("top profile error", top_profile[-N:])
            x = sum((1 for m in bottom_profile[-N:] if m > PT))/N # calculate percentage of out of tolerance conditions
            if x >= X:
                print("bottom profile error", bottom_profile[-N:])


if __name__ == "__main__":
    measure_profile()
